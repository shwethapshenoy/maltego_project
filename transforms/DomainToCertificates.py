from maltego_trx.entities import Certificate
from maltego_trx.transform import DiscoverableTransform

import requests
import json


class DomainToCertificates(DiscoverableTransform):
    """
    Returns Certificate Entities matching the domain.
    """

    @classmethod
    def create_entities(cls, request, response):    
        domain = request.Value    
        
        try:                
            payload = {'CN': domain, 'output': 'json'}
            r = requests.get('https://crt.sh/', params=payload)
            jsonResponse = json.loads(r.text)
            if jsonResponse:
                for cert in jsonResponse:
                    issuerName = cls.getIssuerName(cert.get('issuer_name'))
                    me = response.addEntity(Certificate, issuerName)
                    me.addProperty('id', 'Id', 'strict', cert.get('id'))
                    me.addProperty('issued-to', 'Issued To', 'strict', cert.get('name_value'))
                    me.addProperty('issuer-id', 'Issuer Id', 'strict', cert.get('issuer_ca_id'))
                    me.addProperty('expiry-date', 'Expiry Date', 'strict', cert.get('not_after'))
            else:
                response.addUIMessage("The domain did not return any certificates")
            
        except IOError:
            response.addUIMessage("An error occurred while fetching the certificates.", messageType=UIM_PARTIAL)
            
    @staticmethod    
    def getIssuerName(issuer):
        if 'CN=' not in issuer:
            if 'OU=' not in issuer:
                return ""
            issuerName = issuer.split('OU=')[1] 
        else:
            issuerName = issuer.split('CN=')[1] 
        return issuerName