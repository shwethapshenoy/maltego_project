from maltego_trx.entities import Phrase
from maltego_trx.transform import DiscoverableTransform


class CertificateToIssuer(DiscoverableTransform):
    """
    Returns a phrase with issuer name of the certificate.
    """

    @classmethod
    def create_entities(cls, request, response):
        #issuer = request.getProperty('issuer')
        issuer = request.Value
        
        try:            
            if not issuer:
                response.addUIMessage("The Certificate did not return issuer name")
            else:                
                response.addEntity(Phrase, issuer)
            
        except IOError:
            response.addUIMessage("An error occurred while fetching the issuer name.", messageType=UIM_PARTIAL)